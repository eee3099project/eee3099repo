
//* MODIFIED:  9/19/2018
//*==================================================================*
//* PROGRAMMED IN: Eclipse Luna Service Release 1 (4.4.1)            *
//* DEV. BOARD:    UCT STM32 Development Board                       *
//* TARGET:	   STMicroelectronics STM32F051C6                    *
//*==================================================================*
//* DESCRIPTION:                                                     *
//*                                                                  *
//********************************************************************
// INCLUDE FILES
//====================================================================
#include "lcd_stm32f0.h"
#include "stm32f0xx.h"
#include<string.h>
#include<stdio.h>
#include<stdlib.h>

//TO-DO_lIST
// 1) Check the logic of the line sensors at the moment it is high for on the line and low for off the line								(done)
// 2) Get interrupt working for the start button
// 3) assign a optimal Kp
// 4) get interrupt working for line sensor (IE runs code to stop everything once it is at 10cm)										(implemented but not done)
// 5) fix run program code so that when interrupt is pressed everything happens in order												(done)
// 6) find a offset value that works on the day (adding to it will make it sense a bit further and subtracting will make it closer)
//====================================================================
// SYMBOLIC CONSTANTS
//====================================================================
#define SW0  (GPIO_IDR_0)
#define SW1 (GPIO_IDR_1)
#define line_m (GPIO_IDR_12)//middle
#define line_l (GPIO_IDR_8)//left
#define line_lm (GPIO_IDR_9)//left middle
#define line_rm (GPIO_IDR_11)//right middle
#define line_r (GPIO_IDR_10)//right
// check this again
//====================================================================
// GLOBAL VARIABLES
//====================================================================
uint8_t prox_1;
int result;
int error;							// produced by line sensors
int stop_value;
int integral;
int derivative;
int pre;
int RightMotor;
int LeftMotor;
int PID;

#define Delay1 5000
#define Delay2 3
#define DelayL 60000
#define DelayS 5
#define offset -20
#define kp 8  						//starting at 80% speed highest error =4 therefore 4*5 =20% extra speed
#define ki 0
#define kd 4
#define kpp 10
//====================================================================
// FUNCTION DECLARATIONS
//====================================================================
void init_ports(void);
void init_ADC1(void);
static void init_motors(void);

int get_prox(void);
int proximity_test(int);
int getline(void);
void MotorSpeed(int error);
void runProgram(void);

void Delay(void);
void DelayR(void);
//====================================================================
// MAIN FUNCTION
//====================================================================
void main (void)
{
init_LCD();
init_ports();
init_ADC1();
init_motors();
runProgram();

}

void init_ports(void)
{


//Initialize the GPIOA ports for the push buttons (set to 00 for input)

	RCC->AHBENR |=(RCC_AHBENR_GPIOAEN);	  // enable clock
	GPIOA->MODER &= ~(GPIO_MODER_MODER0); //sets SWO to input device
	GPIOA->MODER &= ~(GPIO_MODER_MODER1); //sets SW1 to input device
	GPIOA->MODER &= ~(GPIO_MODER_MODER2); //sets SW2 to input device
	GPIOA->MODER &= ~(GPIO_MODER_MODER3); //sets SW3 to input device

	GPIOA->MODER |= (GPIO_MODER_MODER5);  //set GPIOA 7 to analog mode (switch to digital for line sensor)
	GPIOA->MODER &= ~(GPIO_MODER_MODER8);
	GPIOA->MODER &= ~(GPIO_MODER_MODER9);  //digital input line left
	GPIOA->MODER &= ~(GPIO_MODER_MODER10); //digital input line left middle
	GPIOA->MODER &= ~(GPIO_MODER_MODER11); //line middle
	GPIOA->MODER &= ~(GPIO_MODER_MODER12); //line right middle
	GPIOA->MODER &= ~(GPIO_MODER_MODER15); //line right




	GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR0_0); //init the PUPDR's for switches
	GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR1_0);
	GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR2_0);
	GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR3_0);

	GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR8_0);
	GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR9_0); // when we use line sensors as digital
	GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR10_0);
	GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR11_0);
	GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR12_0);
	GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR15_0);


	RCC->AHBENR |=(RCC_AHBENR_GPIOBEN);	   // enable clock

	GPIOB->MODER |= (GPIO_MODER_MODER0_0); //set PB0 to output
	GPIOB->MODER |= (GPIO_MODER_MODER1_0); //set PB1 to output
	GPIOB->MODER |= (GPIO_MODER_MODER2_0); //set PB2 to output
	GPIOB->MODER |= (GPIO_MODER_MODER3_0); //set PB3 to output
	GPIOB->MODER |= (GPIO_MODER_MODER4_0); //set PB4 to output
	//GPIOB->MODER |= (GPIO_MODER_MODER5_0); //set PB5 to output
	GPIOB->MODER |= (GPIO_MODER_MODER6_0); //set PB6 to output
	GPIOB->MODER |= (GPIO_MODER_MODER7_0); //set PB7 to output
	GPIOB->MODER |= (GPIO_MODER_MODER8_0); //set PB7 to output

	GPIOB -> ODR &= 0b000000000000000;				// set jonti's pins to high
}//init ports

void init_ADC1(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_ADCEN; //enable clock for ADC
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN; //enable clock for port which ADC samples from
	GPIOA->MODER |= GPIO_MODER_MODER5; //set PA5 to analogue mode
	ADC1->CHSELR |= ADC_CHSELR_CHSEL5; // select channel 5
	ADC1->CFGR1 |= ADC_CFGR1_RES_1;    // resolution to 8 bit
	ADC1->CR |= ADC_CR_ADCAL;         // set ADCAL high and wait for it to go low
	while( (ADC1->CR & ADC_CR_ADCAL) != 0);
	ADC1->CR |= ADC_CR_ADEN;           // set ADEN=1 in the ADC_CR register
	while((ADC1->ISR & ADC_ISR_ADRDY) == 0); //wait until ADRDY==1 in ADC_ISR

}

static void init_motors(void) {
  // initialise the timer to generate PWM
  RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

  GPIOB->MODER |= (GPIO_MODER_MODER10_1); 	//was 10				// PB10 = AF for the pwm    //left motor
  GPIOB->MODER |= (GPIO_MODER_MODER11_1); 	//was 11				// PB11 = AF for the pwm	  //right motor

  //map pins 10 and 11 to individual channels within timer2  SOMETHING ABOUT TIMERS
  GPIOB->AFR[1] |= (2 << (4*(10 - 8))); 					// PB10_AF = AF2 (ie: map to TIM2_CH3)
  GPIOB->AFR[1] |= (2 << (4*(11 - 8))); 					// PB11_AF = AF2 (ie: map to TIM2_CH4)

  TIM2->ARR = 8000; 										//frequency // Was 8000
  TIM2->PSC = 0;
  // specify PWM mode: OCxM bits in CCMRx. We want mode 1
  TIM2->CCMR2 |= (TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1); 	// PWM Mode 1
  TIM2->CCMR2 |= (TIM_CCMR2_OC4M_2 | TIM_CCMR2_OC4M_1); 	// PWM Mode 1


  // enable the OC channels
  TIM2->CCER |= TIM_CCER_CC3E;
  TIM2->CCER |= TIM_CCER_CC4E;

  TIM2->CR1 |= TIM_CR1_CEN; 								// counter enable

  TIM2->CCR3 = 0; 				// changing duty cycle // change for jonti pwm
  TIM2->CCR4 = 0;					// changing duty cycle

}

//====================================================================
// I/O Functions
//====================================================================

int get_prox(void)
{

ADC1->CR |= ADC_CR_ADSTART;
// wait for end of conversion: EOC == 1. Not necessary to clear EOC as we read from DR
while((ADC1->ISR & ADC_ISR_EOC) == 0);

stop_value = ADC1 ->DR;
return stop_value;

}

int proximity_test(int stop_value)
{
	int dontStop = 0;
	int stop = 2;

	ADC1->CR |= ADC_CR_ADSTART;// wait for end of conversion: EOC == 1. Not necessary to clear EOC as we read from DR
	while((ADC1->ISR & ADC_ISR_EOC) == 0);
	prox_1 = ADC1->DR;

	if (prox_1 > stop_value)
		{
		result = dontStop;
		}
	if (prox_1 <= stop_value)
		{
		result  = stop;
		}
	return result;
}

int getline(void)
{
	if (((GPIOA->IDR & line_l)==0) && ((GPIOA->IDR & line_lm)==0) && ((GPIOA->IDR & line_m)==0) && ((GPIOA->IDR & line_rm)==0) && ((GPIOA->IDR & line_r) !=0))
				{
				error = 7;
				//11110
				}
	else if (((GPIOA->IDR & line_l)==0) && ((GPIOA->IDR & line_lm)==0) && ((GPIOA->IDR & line_m)==0) && ((GPIOA->IDR & line_rm)!=0) && ((GPIOA->IDR & line_r) !=0))
				{
				error = 5;
				}//11100
	else if (((GPIOA->IDR & line_l)==0) && ((GPIOA->IDR & line_lm)==0) && ((GPIOA->IDR & line_m)==0) && ((GPIOA->IDR & line_rm)!=0) && ((GPIOA->IDR & line_r) ==0))
				{
				error = 2;
				}//11101
	else if (((GPIOA->IDR & line_l)==0) && ((GPIOA->IDR & line_lm)==0) && ((GPIOA->IDR & line_m)!=0) && ((GPIOA->IDR & line_rm)!=0) && ((GPIOA->IDR & line_r) ==0))
				{
				error = 1;
				}//11001
	else if (((GPIOA->IDR & line_l)==0) && ((GPIOA->IDR & line_lm)==0) && ((GPIOA->IDR & line_m)!=0) && ((GPIOA->IDR & line_rm)==0) && ((GPIOA->IDR & line_r) ==0))
				{
				error = 0;
				}//11011

	else if (((GPIOA->IDR & line_l)==0) && ((GPIOA->IDR & line_lm)!=0) && ((GPIOA->IDR & line_m)!=0) && ((GPIOA->IDR & line_rm)==0) && ((GPIOA->IDR & line_r) ==0))
				{
				error = -1;
				}//10011
	else if (((GPIOA->IDR & line_l)==0) && ((GPIOA->IDR & line_lm)!=0) && ((GPIOA->IDR & line_m)==0) && ((GPIOA->IDR & line_rm)==0) && ((GPIOA->IDR & line_r) ==0))
				{
				error = -2;
				}//10111
	else if (((GPIOA->IDR & line_l)!=0) && ((GPIOA->IDR & line_lm)!=0) && ((GPIOA->IDR & line_m)==0) && ((GPIOA->IDR & line_rm)==0) && ((GPIOA->IDR & line_r) ==0))
				{
				error = -5;
				}//00111
	else if (((GPIOA->IDR & line_l)!=0) && ((GPIOA->IDR & line_lm)==0) && ((GPIOA->IDR & line_m)==0) && ((GPIOA->IDR & line_rm)==0) && ((GPIOA->IDR & line_r) ==0))
				{
				error = -7;
				}//01111
	else if (((GPIOA->IDR & line_l)==0) && ((GPIOA->IDR & line_lm)==0) && ((GPIOA->IDR & line_m)==0) && ((GPIOA->IDR & line_rm)==0) && ((GPIOA->IDR & line_r) ==0))
				{
				Delay(); // can make this delay shorter
				if (pre*pre > 49)
				{
				error = 10;
				}
				 // need to implement something in the duty cycle that makes the robot swing round
				}//11111
	else
		{
		error = 0;
		}


	return error;
}

void MotorSpeed(int error)
{
	//logic if sensing on right needs to then turn right
	// changes duty cycle based on the error.// not sure if this is correct need to check with motors.


	derivative = error - pre;
	PID = (kpp*error) + (kd*derivative);

	if (error==0)
	{
		RightMotor = (100*80);//80 is the full therefore this is 80% speed
		LeftMotor  = (100*80);
	}
	else if (error==10 && pre <0) // no line detected
	{
		RightMotor = (40*80);  // since the track only has a corner going in one direction if we don't see a line we need to speed up right motor to get back on the line
		LeftMotor  = (0*80);
	}
	else if (error==10 && pre >0) // no line detected
	{
		RightMotor = (0*80);  // since the track only has a corner going in one direction if we don't see a line we need to speed up right motor to get back on the line
		LeftMotor  = (40*80);
	}

	else if (error==8) // stop condition
	{
		RightMotor = (0*80);
		LeftMotor  = (0*80);
	}
	else
	{
		RightMotor = ((100*80 +(2*PID*80)));
		LeftMotor  = ((100*80 -(2*PID*80)));
//		RightMotor = ((70*80 +(kp*error*80)));
//		LeftMotor  = ((70*80 -(kp*error*80)));
	}

	TIM2->CCR3 = RightMotor; 				// changing duty cycle
	TIM2->CCR4 = LeftMotor;					// changing duty cycle

	pre = error;
}

void Delay(void)
{
	volatile int i,j;						//makes ints that are able to change(volatile)
	for (i=0; i<=Delay1; i++)
		{
			__asm("nop");									//loop 1
			for(j=0; j<=Delay2; j++)
				{
					__asm("nop");							//loop 2
				}
		}
}//delayfn

void DelayR(void) 							// slightly longer than Delay
{
	volatile int i,j;						//makes ints that are able to change(volatile)
	for (i=0; i<=DelayL; i++);				//loop 1
	for(j=0; j<=DelayS; j++); 				//loop 2
}//delayfn

//====================================================================
// CONTROLLER
//====================================================================
void runProgram(void)
{
	while(1)
	{
		if ((GPIOA->IDR & SW0)==0)
		{
			//stop_value = 80;//sets a value from the prox calibration
			while ((GPIOA->IDR & SW0)==0)
			{
				get_prox();
			}
			while(2)
			{
				if ((GPIOA->IDR & SW0)==0)
				{
					pre = 0;
					GPIOB -> ODR |= 0b000000011000000;				// set jonti's pins to high
					GPIOB -> ODR &=~0b000000100000000;				// set jonti's pins to high
					int state = 1;
					int run = 1;
					while(state==1)
					{
						if ((proximity_test(stop_value))==2)
						{
							error = 8;								//if error == 8 motor stops
							MotorSpeed(error); 						// stops motors
							run = 0;								// stops line sensing
						}

						if (run == 1)
						{
							MotorSpeed(getline());
						} //run if
					}// while state
				}// if number 2
			} // while 2
		} // if 1st button press  //while 1
	} // while 1
}// void run

//********************************************************************
// END OF PROGRAM
//********************************************************************

